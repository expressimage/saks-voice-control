const AWS = require('aws-sdk');
const validator = require('validator');

const sns = new AWS.SNS();
const lambda = new AWS.Lambda({
	region: 'us-east-1'
});

const sendSMS = (toPhone, body) => {
    return new Promise((resolve, reject) => {
        const params = {
            Message: body,
            MessageStructure: 'string',
            PhoneNumber: toPhone,
        };

        sns.publish(params, (err, data) => {
            if (err) {
                console.log('sendSMS error', err);

                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

const shortenLink = (fullLink) => {
    return new Promise((resolve, reject) => {
        lambda.invoke({
            FunctionName: 'wayfinding-platform-shorten-link',
            Payload: JSON.stringify({ longurl:fullLink })
        }, function(error, data) {
            if (error) {
                reject(error);
            } else {
                resolve(data.Payload);
            }
        });
    });
};

exports.handler = async (event) => {
    let phoneNumber = event.phone_number;

    phoneNumber = phoneNumber.includes('+') ? phoneNumber : `+1${phoneNumber}`;

    const startingFeature = event.starting_feature_id;
    const endingFeature = event.ending_feature_id;
    const endingNavNode = event.ending_nav_node;
    const facingDirection = event.facing_direction;
    const ada = event.ada;
    const appVersionName = event.app_version_name;

    let stage;

    if (appVersionName) {
        stage = appVersionName.substring(appVersionName.indexOf('('));
        
        if (stage === '(Dev)' || stage === 'UNKNOWN') {
            stage = 'development';
        } else if (stage === '(Staging)') {
            stage = 'staging';
        } else {
            stage = 'production';
        }
    } else {
        stage = 'development';
    }
    
    const endingNavNodeString = endingNavNode ? `/${endingNavNode}` : '';

    const fullLink = `https://saks.expressimage.digital/${stage}/ttpIndex.html#/wayfinder/${startingFeature}/${endingFeature}/${facingDirection}/${ada}${endingNavNodeString}`;

    console.log('fullLink', fullLink);

    const shortenedLink = await shortenLink(fullLink);

    console.log('shortenedLink', shortenedLink);

    const textMessage = `Please see your mobile wayfinding link below:\r\n\r\n${shortenedLink}`;

    console.log('textMessage', textMessage);

    if (phoneNumber && validator.isMobilePhone(phoneNumber)) {
        try {
            const smsResult = await sendSMS(phoneNumber, textMessage);

            console.log('smsResult', smsResult);

            return true;
        } catch (e) {
            throw e;
        }
    } else {
        throw new Error('Please pass in a valid email address or phone number');
    }
};
