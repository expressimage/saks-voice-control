import { WebviewParentHandler } from '@express-image-digital/platform-chrome-app-library';

const webviewParentHandlerOptions = {
  fallbackDeviceId: process.env.VUE_APP_FALLBACK_DEVICE_ID || '',
  heightOverride: 1920,
  widthOverride: 1080,
};

const webviewParentHandlerInstance = new WebviewParentHandler(webviewParentHandlerOptions);

webviewParentHandlerInstance.initialize();
