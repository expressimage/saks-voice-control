<template>
  <div 
    class="footer" 
    v-bind:class="{ 
        'footer_hasFadeout': showFooterButtons,
        'footer_ada' : isAda,
      }
  ">
    <div class="quickNav" v-if="showQuickNav" v-bind:class="{ 'quickNav_ada': isAda }">
        <button class="quickNav-btn" @click="$router.push({ path: '/restrooms' })">
          <icon-base class="quickNav-btn-icon" :width="52.9" :height="44.6" iconColor="white" stroke="black"><RestroomsIcon /></icon-base>
          <div class="quickNav-btn-text">{{ restroomsText }}</div>
        </button>
        <button class="quickNav-btn" @click="goToCustomerService">
          <icon-base class="quickNav-btn-icon" :width="33.8" :height="40.1" iconColor="white" stroke="black"><CustomerServiceIcon /></icon-base>
          <div class="quickNav-btn-text">{{ customerServiceText }}</div>
        </button>
        <button class="quickNav-btn" @click="goToATM">
          <icon-base class="quickNav-btn-icon" :width="43.3" :height="35.2" iconColor="white" stroke="black"><AtmIcon /></icon-base>
          <div class="quickNav-btn-text">{{ atmText }}</div>
        </button>
    </div>
    <div class="backHomeBtns" v-if="showFooterButtons">
        <button class="btn" v-on:click="goBack">{{ backText }}</button>
        <button class="btn" v-on:click="$router.push({ path: '/home' })">{{ homeText }}</button>
    </div>
  </div>
</template>

<style lang="scss" scoped>
  .footer {
    background: #fff;
    border-bottom: 22px solid #e6e6e6;
    bottom: 0;
    direction: ltr;
    left: 50%;
    max-width: 1080px;
    padding: 35px 0;
    position: fixed;
    transform: translateX(-50%);
    width: 100%;
  }

  .footer_ada {
    padding: 10px 0 30px;
  }

  .footer_hasFadeout:before {
    content: '';
    background-image: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,1));
    height: 60px;
    left: 0;
    pointer-events: none;
    position: absolute;
    width: 100%;
    top: -59px;
  }
  
  .backHomeBtns {
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
    width: 77.777%;

    & > * {
      flex: 1 0 calc(50% - 12px);

      &:nth-child(even) {
        margin-left: 12px;
      }
    }
  }

  .quickNav {
    align-items: stretch;
    display: flex;
    margin: 50px auto;
    width: 77.777%;

    & > * {
      background: transparent;
      border: 0; // needed reset
      border-left: 1px solid #000;
      flex: 1 0 33.333%;
      padding: 10px 0;

      &:first-child {
        border-left: 0;
      }
    }

    .quickNav-btn-icon {
      margin-bottom: 20px;
      width: auto;
    }

    .quickNav-btn-text {
      font-family: 'Gotham-Medium';
      font-size: 14px;
      text-transform: uppercase;
    }
  }

  .quickNav_ada {
    margin: 0 auto 20px;
  }
  
</style>

<script>
import AtmIcon from '@/components/Icons/Atm.vue';
import CustomerServiceIcon from '@/components/Icons/CustomerService.vue';
import RestroomsIcon from '@/components/Icons/Restrooms.vue';

export default {
  name: 'app-footer',
  components: {
    AtmIcon,
    CustomerServiceIcon,
    RestroomsIcon,
  },
  props: {
    showFooterButtons: {
      type: Boolean,
      required: true,
    },
    showQuickNav: {
        type: Boolean,
        required: true,
    },
    isAda: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      backText: '',
      homeText: '',
      atmText: '',
      customerServiceText: '',
      restroomsText: '',
      voiceCommands: [],
      subVoiceCommands: [],
    };
  },
  async created() {
    this.backText = await this.getDynamicTranslation('back');
    this.homeText = await this.getDynamicTranslation('home');
    this.atmText = await this.getDynamicTranslation('ATM');
    this.customerServiceText = await this.getDynamicTranslation('Customer Service');
    this.restroomsText = await this.getDynamicTranslation('restrooms');
  },
  mounted() {
    this.voiceCommands = [
      {
        indexes: [ 'home', 'go home', 'go to home', 'go to the beginning' ],
        action: () => {
          if (this.$router.currentRoute.name !== 'home') {
            this.$router.push({ path: '/home' });
          }
        },
      },
      {
        indexes: [ 'back', 'go back', 'previous page' ],
        action: () => {
          window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/');
        },
      },
    ];

    this.subVoiceCommands = [
      {
        indexes: [ 'restrooms', 'restroom', 'bathroom', 'bathrooms' ],
        action: () => {
          this.$router.push({ path: '/restrooms' });
        },
      },
      {
        indexes: [ 'customer service', 'customer', 'help me', 'help', 'helpdesk' ],
        action: () => {
          this.goToCustomerService();
        },
      },
      {
        indexes: [ 'atm', 'cash', 'money' ],
        action: () => {
          this.goToATM();
        },
      },
    ];

    this.addVoiceCommands(this.voiceCommands);
  },
  methods: {
    goBack() {
      // debugger;
      window.allowHistory = false;
      window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/');
    },
    goToATM() {
      const pois = this.$store.getters['appData/POIS'];

      const atmPOI = pois.find((poi) => poi.name.toLowerCase() === 'atm' && poi.type.toLowerCase() === 'amenity');

      this.$router.push({
        name: 'wayfinder',
        params: { to: atmPOI.id },
      });
    },
    goToCustomerService() {
      const pois = this.$store.getters['appData/POIS'];

      const customerServicePOI = pois.find((poi) => poi.name.toLowerCase() === 'customer service');

      this.$router.push({
        name: 'wayfinder',
        params: { to: customerServicePOI.id },
      });
    },
  },
  watch: {
    async currentLanguage(newVal) {
      this.backText = await this.getDynamicTranslation('back');
      this.homeText = await this.getDynamicTranslation('home');
      this.atmText = await this.getDynamicTranslation('ATM');
      this.customerServiceText = await this.getDynamicTranslation('Customer Service');
      this.restroomsText = await this.getDynamicTranslation('restrooms');
    },
    showQuickNav(newVal) {
      if (newVal === true) {
        this.addVoiceCommands(this.subVoiceCommands);
      } else if (newVal === false) {
        this.subVoiceCommands.forEach((voiceCommand) => {
            this.removeVoiceCommand(voiceCommand.indexes);
        });
      }
    },
  },
};
</script>
