import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import localForage from 'localforage';
import App from '@/TtpApp.vue';
import router from '@/router/ttpRouter';
import appData from './store/modules/appData';
import IconBase from '@/components/IconBase.vue';
import { Plugin } from 'vue-fragment';

Vue.config.productionTip = false;

Vue.component('icon-base', IconBase);

Vue.use(Plugin);
Vue.use(Vuex);

localForage.config({
  driver      : localForage.INDEXEDDB,
  name        : 'localState',
  version     : 1.0,
});

const vuexLocal = new VuexPersistence({
  storage: localForage,
  asyncStorage: true,
});

(window as any).vuexStore = new Vuex.Store({
  modules: {
    appData,
  },
  plugins: [vuexLocal.plugin],
});

new Vue({
  store: (window as any).vuexStore,
  router,
  methods: {
    loadAppData() {
      console.log('Getting all app data...');

      this.$store.dispatch('appData/GET_ALL_DATA');
    },
  },
  created() {
    this.loadAppData();
  },
  render: (h) => h(App),
}).$mount('#app');
