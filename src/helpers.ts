import { TouchLog } from '@express-image-digital/platform-chrome-app-library/dist/types/types';
import classifyPoint from 'robust-point-in-polygon';
import { Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import { POI } from './types';

export const logError = (err: any) => {
  (Vue as any).rollbar.error(err);
};

export const logDebug = (message: any) => {
  (Vue as any).rollbar.debug(message);
};

export const setLanguage = (languageCode: string) => {
  (window as any).EIDChromeLibrary.vuexStore.commit('appData/SET_CURRENT_LANGUAGE', languageCode);

  logTouch({
    device_name: (window as any).initParam,
    touch_type: 'Language Switch',
    raw_data: {
      lang: languageCode,
    },
  });
};

export const getNumberPosition = (numWord: string): { word: string, num: string } => {
  switch (numWord) {
    case 'one':
      return { word: 'first', num: '1st' };
    case 'two':
      return { word: 'second', num: '2nd' };
    case 'three':
      return { word: 'third', num: '3rd' };
    case 'four':
      return { word: 'fourth', num: '4th' };
    case 'five':
      return { word: 'fifth', num: '5th' };
    case 'six':
      return { word: 'sixth', num: '6th' };
    case 'seven':
      return { word: 'seventh', num: '7th' };
    case 'eight':
      return { word: 'eigth', num: '8th' };
    case 'nine':
      return { word: 'ninth', num: '9th' };
    case 'ten':
      return { word: 'tenth', num: '10th' };
    default:
      return { word: numWord, num: numWord };
  }
};

export const addVoiceCommands = (commands: Array<{ indexes: string[], action: any, smart?: boolean }>): void => {
  // console.log('addVoiceCommands', commands);

  (window as any).artyom.addCommands(commands);
};

export const removeVoiceCommand = (index: string[]): void => {
  // console.log('removeVoiceCommand', index);

  // tslint:disable-next-line: max-line-length
  const currentCommandsArray: Array<{ indexes: string[], action: any, smart?: boolean }> = (window as any).artyom.ArtyomCommands;

  const commandArrayIndexesToDelete: number[] = [];

  currentCommandsArray.forEach((currentCommand, arrIndex) => {
    if (currentCommand.indexes.join(',') === index.join(',')) {
      commandArrayIndexesToDelete.push(arrIndex);
    }
  });

  commandArrayIndexesToDelete.forEach((arrIndex) => {
    (window as any).artyom.ArtyomCommands.splice(arrIndex, 1);
  });
};

export const addBaseVoiceCommands = (router: VueRouter) => {
  addVoiceCommands([
    {
      indexes: [ 'toggle ada mode', 'ada mode', 'turn on ada mode', 'turn off ada mode', 'toggle accessible mode', 'accessible mode', 'accessibility', 'lower content', 'raise content', 'move content down'  ],
      action: () => {
        (window as any).EIDChromeLibrary.vuexStore.dispatch('appData/TOGGLE_ADA');
      },
    },
    {
      indexes: [ 'Set the language to *', 'Set language to *', 'Change the language to *', 'Change language to *' ],
      smart: true,
      action: (i: any, wildcard: any) => {
        switch (wildcard) {
          case 'english':
            setLanguage('en');

            break;
          case 'spanish':
            setLanguage('es');

            break;
          case 'chinese':
            setLanguage('zh');

            break;
          case 'german':
            setLanguage('de');

            break;
          case 'arabic':
            setLanguage('ar');

            break;
          case 'russian':
            setLanguage('ru');

            break;
          case 'tagalog':
            setLanguage('tl');

            break;
          case 'korean':
          case 'south korean':
            setLanguage('ko');

            break;
          case 'japanese':
            setLanguage('ja');

            break;
          case 'italian':
            setLanguage('it');

            break;
          case 'french':
            setLanguage('fr');

            break;
          case 'portugese':
            setLanguage('pt');

            break;
        }
      },
    },
    {
      indexes: [ 'stop' ],
      action: () => {
        (window as any).artyom.dontObey();
      },
    },
    {
      indexes: [  'search for *', 'navigate to *', 'wayfind to *', 'search *',
                  'find me *', 'i want *', 'what * are here', 'what * is here',
                  'how do i get to *', 'i want to go to *' ],
      smart: true,
      action: (i: any, wildcard: string) => {
        switch (wildcard) {
          case 'designers':
            router.push({ path: '/designers' });

            break;
          case 'categories':
            router.push({ path: '/categories' });

            break;
          case 'food':
          case 'restaurants':
          case 'dining':
            router.push({ path: '/dining' });

            break;
          default:
            router.push({ path: `/search/${wildcard}` });

            break;
        }
      },
    },
  ]);
};

export const getFilenameFromURL = (fileURL: string) => {
  let fileName = (fileURL.indexOf('?') === -1) ?
    fileURL.substring(fileURL.lastIndexOf('/') + 1) :
    fileURL.substring(fileURL.lastIndexOf('/') + 1, fileURL.indexOf('?'));

  fileName = fileName.replace(/[^a-zA-Z0-9]/g, '');

  return fileName;
};

export const logTouch = async (touchLog: TouchLog): Promise<void> => {
  await (window as any).EIDChromeLibrary.touchLogging.logTouch(touchLog);

  return;
};

export const newSession = async (): Promise<void> => {
  await (window as any).EIDChromeLibrary.touchLogging.newSession();

  return;
};

export const clearDeviceDataAndFilesystem = async (): Promise<void> => {
  await (window as any).EIDChromeLibrary.vuexStore.commit('device/RESET_DEVICE_DATA');

  return;
};

export const restartDevice = async (): Promise<void> => {
  (window as any).EIDChromeLibrary.appWebviewHandler.sendMessage('restartChrome');
};

export const logGenericTouch = (e: any): void => {
  if (e.target && e.target.id && e.target.id.length === 0 && e.target.className === 0) {
    return;
  }

  let src = '';

  if (e.target.src) {
    src = e.target.src.split('/');
    src = src[src.length - 1];
  }

  const toTrack = e.target.localName + '|' + e.target.id + '|' +
    e.target.className + (typeof e.target.src !== 'undefined' ? '|' + src : '');

  if (toTrack.length < 5) {
    return;
  }

  if (toTrack !== (window as any).prevGenericTouch) {
    logTouch({ device_name: (window as any).initParam, touch_type: 'Generic', raw_data: { generic: toTrack } });
  }

  (window as any).prevGenericTouch = toTrack;

  return;
};

export const isAlpha = (ch: string) => {
    return  typeof ch === 'string' && ch.length === 1
            && (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z');
};

export const inside = (point: number[], vs: number[][]) => {
  const x = point[0];
  const y = point[1];

  let insidePoly = false;

  for (let i = 0, j = vs.length - 1; i < vs.length; j = i++) {
      const xi = vs[i][0];
      const yi = vs[i][1];

      const xj = vs[j][0];
      const yj = vs[j][1];

      const intersect = ((yi > y) !== (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) { insidePoly = !insidePoly; }
  }

  return insidePoly;
};

export const insideRobust = (point: number[], vs: number[][]) => {
  const pointSpecification = classifyPoint(vs, point);

  return (pointSpecification <= 0);
};

export const getFirstWord = (phrase: string) => {
  return phrase.replace(/\s.*/, '');
};

export const getLargestLine = (coordinates: any) => {
  let maxLength = 0;
  let prevPnt: any = [];
  let side: any = [];

  coordinates.forEach((pnt: any) => {
    if (prevPnt.length !== 0) {
      const length = distance(pnt, prevPnt);
      if (length > maxLength) {
        maxLength = length;
        side = [prevPnt, pnt];
      }
    }

    prevPnt = pnt;
  });

  return side;
};

export const getCenter = (coordinates: any[]) => {
    let minX = 1000;
    let minY = 1000;
    let maxX = -1000;
    let maxY = -1000;

    coordinates.map((value) => {
      if (value[0] < minX) {
        minX = value[0];
      }

      if (value[1] < minY) {
        minY = value[1];
      }

      if (value[0] > maxX) {
        maxX = value[0];
      }

      if (value[1] > maxY) {
        maxY = value[1];
      }
    });

    const halfHeight = (maxY + minY) / 2;

    let topRight = [-1000, -1000];
    let topLeft = [1000, -1000];
    let bottomRight = [-1000, 1000];
    let bottomLeft = [1000, -1000];

    coordinates.map((value) => {
      if (value[1] < halfHeight) {
        if (value[0] < bottomLeft[0]) {
          bottomLeft = value;
        }

        if (value[0] > bottomRight[0]) {
          bottomRight = value;
        }
      }

      if (value[1] > halfHeight) {
        if (value[0] < topLeft[0]) {
          topLeft = value;
        }

        if (value[0] > topRight[0]) {
          topRight = value;
        }
      }
    });

    const middleX = (topLeft[0] + bottomRight[0]) / 2;
    const middleY = (topLeft[1] + bottomRight[1]) / 2;

    return [ middleX, middleY ];
};

export const distance = (pnt1: any, pnt2: any) => {
  const radlat1 = Math.PI * pnt1[0] / 180;
  const radlat2 = Math.PI * pnt2[0] / 180;

  const theta = pnt1[1] - pnt2[1];
  const radtheta = Math.PI * theta / 180;

  let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

  dist = Math.acos(dist);
  dist = dist * 180 / Math.PI;

  return dist;
};

export const getSlope = (side: any) => {
  const pnt1 = side[0];
  const pnt2 = side[1];

  const x1 = pnt1[0];
  const x2 = pnt2[0];

  const y1 = pnt1[1];
  const y2 = pnt2[1];

  const slope = (y2 - y1) / (x2 - x1);

  return slope;
};

export const stringDivider = (str: string, width: number, spaceReplacer: string): string => {
  if (str.length > width) {
    let p = width;
    while (p > 0 && (str[p] !== ' ' && str[p] !== '-')) {
      p--;
    }
    if (p > 0) {
      let left;
      if (str.substring(p, p + 1) === '-') {
        left = str.substring(0, p + 1);
      } else {
        left = str.substring(0, p);
      }
      const right = str.substring(p + 1);
      return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
    }
  }
  return str;
};

export const getCategoryLevelString = (categoryPOIs: POI[]) => {
  let allLevels: any[] = [];

  categoryPOIs.forEach((poi) => {
    poi.levels.forEach((level) => {
      if (!allLevels.includes(level.number)) {
        allLevels.push(level.number);
      }
    });
  });

  allLevels = allLevels.sort();

  if (allLevels.length === 0) {
    return '';
  }

  let levelString;
// TODO: How can we translate? This may need to be in global.ts
  if (allLevels.includes(-1) && allLevels.length === 1) {
    levelString = 'Lower Level';

    allLevels.shift();
  } else if (allLevels.includes(-1) && allLevels.length === 2) {
    levelString = 'Lower Level & Level ';

    allLevels.shift();
  } else if (allLevels.includes(-1)) {
    levelString = 'Lower Level, Level ';

    allLevels.shift();
  } else {
    levelString = 'Level ';
  }

  for (let i = 0; i < allLevels.length; i++) {
    if (allLevels.length === 1 || i === 0) {
      levelString += (allLevels[i] + 1);
    } else if (i === allLevels.length - 1) {
      levelString += ` & ${allLevels[i] + 1}`;
    } else {
      levelString += `, ${allLevels[i] + 1}`;
    }
  }

  return levelString;
};

export const distanceBetween = (a: number[], b: number[]) => {
  return Math.abs(
    Math.sqrt(
      Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2),
    ),
  );
};

export const getNearestPOI = (pois: POI[], startingPoint: string) => {
  const allPOIs: any = [];

  pois.forEach((poi) => {
    if (poi.navNodes) {
      poi.navNodes.forEach((poiNavNode) => {
        allPOIs.push({
          id: poi.id,
          navNode: poiNavNode.navNode,
        });
      });
    }
  });

  const nearestPOI = allPOIs.reduce((nearest: any, restroom: any) => {
    const route = getRoute(startingPoint, restroom.navNode, false);

    const distanceInternal = (route && route.distance) ? route.distance : 99999;

    if (nearest.distance === null || distanceInternal < nearest.distance) {
      nearest = {
        id: restroom.id,
        navNode: restroom.navNode,
        distance: distanceInternal,
      };
    }

    return nearest;
  });

  return nearestPOI;
};

export const radToDeg = (rad: number) => {
  return rad * (180 / Math.PI);
};

export const degToRad = (deg: number) => {
  return deg * (Math.PI / 180);
};

export const translateMinMax = (input: number, inputLow: number, inputHigh: number,
                                outputHigh: number, outputLow: number) => {
  return ((input - inputLow) / (inputHigh - inputLow)) * (outputHigh - outputLow) + outputLow;
};

export const match = (needle: number, haystack: any, fallback: any) => {
  const matchRes = haystack.find((item: any) => needle === item[0]);
  if (matchRes !== undefined) {
    return matchRes[1];
  } else if (fallback !== undefined) {
    return fallback;
  } else {
    return null;
  }
};

export const eventDateFormat = (item: any) => {
  const date = new Date(item.year, item.month - 1, item.day);

  if (item.hours !== undefined) {
    date.setHours(item.hours, item.minutes, item.seconds);
  }

  return formatDate(date);
};

export const formatDate = (dateObject: Date) => {
  const startDate = new Date(dateObject);

  const startDay = match(startDate.getDay(), [
    [0, 'Sun.'],
    [1, 'Mon.'],
    [2, 'Tue.'],
    [3, 'Wed.'],
    [4, 'Thu.'],
    [5, 'Fri.'],
    [6, 'Sat.'],
  ], '');

  const startMon = match(startDate.getMonth(), [
    [0, 'Jan.'],
    [1, 'Feb.'],
    [2, 'Mar.'],
    [3, 'Apr.'],
    [4, 'May'],
    [5, 'Jun.'],
    [6, 'Jul.'],
    [7, 'Aug.'],
    [8, 'Sep.'],
    [9, 'Oct.'],
    [10, 'Nov.'],
    [11, 'Dec.'],
  ], '');

  let hour = startDate.getHours();
  const ampm = hour >= 12 ? 'PM' : 'AM';
  if (hour > 12) {
    hour -= 12;
  } else if (hour === 0) {
    hour = 12;
  }

  let minutes = startDate.getMinutes().toString();
  if (minutes.length === 1) { minutes = '0' + minutes; }

  return `${startDay}, ${startMon} ${startDate.getDate()}, ${hour}:${minutes} ${ampm}`;
};

export const formatTimeAndDate = (now: Date) => {
  let hour = now.getHours();
  let minute = String(now.getMinutes());
  let amPm = 'AM';

  if (hour >= 12) {
    amPm = 'PM';
    hour -= 12;
  }

  if (hour === 0) {
    hour = 12;
  }

  if (minute.length === 1) {
    minute = '0' + minute;
  }

  const date = now.getDate();

  const day = match(now.getDay(), [
    [0, 'Sunday'],
    [1, 'Monday'],
    [2, 'Tuesday'],
    [3, 'Wednesday'],
    [4, 'Thursday'],
    [5, 'Friday'],
    [6, 'Saturday'],
  ], '');

  const month = match(now.getMonth(), [
    [0, 'Jan.'],
    [1, 'Feb.'],
    [2, 'Mar.'],
    [3, 'Apr.'],
    [4, 'May'],
    [5, 'June'],
    [6, 'July'],
    [7, 'Aug.'],
    [8, 'Sep.'],
    [9, 'Oct.'],
    [10, 'Nov.'],
    [11, 'Dec.'],
  ], '');

  const year = now.getFullYear();

  return {
    time: `${hour}:${minute} ${amPm}`,
    date: `${month} ${date}, ${year}`,
  };
};



export const getLevelName = (level: number) => {
  return match(level, [
    [1, 'Level 1'],
    [-1, 'East Plaza'],
    [-2, 'Kellogg Entry'],
    [-3, 'Parking'],
    [2, 'Level 2'],
    [3, 'Level 3'],
  ], '');
};

export const getLevelColor = (level: number) => {
  return match(level, [
    [1, 'blue'],
    [-1, 'mauve'],
    [-2, 'orange'],
    [-3, 'gray'],
    [2, 'green'],
    [3, 'darkblue'],
  ], 'blue');
};

export const getColorByIndex = (index: number) => {
  return match(index % 4, [
    [0, 'orange'],
    [1, 'mauve'],
    [2, 'blue'],
    [3, 'green'],
  ], 'orange');
};

export const scrollHandler = (vnode: any) => {
  const parent = vnode.dom;
  const child = parent.childNodes[0];

  function handleScroll() {
    const tallEnough = child.scrollHeight > child.clientHeight;

    // add the images?
    parent.classList[child.scrollTop > 20 && tallEnough ? 'add' : 'remove']('shadow-top');
    parent.classList[child.scrollHeight - child.clientHeight - child.scrollTop > 20 &&
      tallEnough ? 'add' : 'remove']('shadow-bottom');
  }

  child.onscroll = handleScroll;
  handleScroll();

  vnode.attrs.onremove = () => {
    child.removeEventListener('scroll', handleScroll);
  };
};

export const searchFor = (mapPlaces: any, query: string) => {
  function damerau_levenshtein(source: string, target: string) {
    if (!source) { return target ? target.length : 0; } else if (!target) { return source.length; }

    const m = source.length;
    const n = target.length;
    const INF = m + n;
    const score = new Array(m + 2);
    const sd: any = {};

    for (let i = 0; i < m + 2; i++) { score[i] = new Array(n + 2); }

    score[0][0] = INF;

    for (let i = 0; i <= m; i++) {
      score[i + 1][1] = i;
      score[i + 1][0] = INF;
      sd[source[i]] = 0;
    }

    for (let j = 0; j <= n; j++) {
      score[1][j + 1] = j;
      score[0][j + 1] = INF;
      sd[target[j]] = 0;
    }

    for (let i = 1; i <= m; i++) {
      let DB = 0;

      for (let j = 1; j <= n; j++) {
        const i1 = sd[target[j - 1]];
        const j1 = DB;

        if (source[i - 1] === target[j - 1]) {
          score[i + 1][j + 1] = score[i][j];
          DB = j;
        } else {
          score[i + 1][j + 1] = Math.min(score[i][j], Math.min(score[i + 1][j], score[i][j + 1])) + 1;
        }

        score[i + 1][j + 1] = Math.min(score[i + 1][j + 1],
          score[i1] ? score[i1][j1] + (i - i1 - 1) + 1 + (j - j1 - 1) : Infinity);
      }

      sd[source[i - 1]] = i;
    }

    return score[m + 1][n + 1];
  }

  function normalize(phrase: string) {
    return String(phrase)
      .toLowerCase()
      .replace('\'', '')
      .replace('’', '')
      .replace('é', 'e')
      .replace('ä', 'a')
      .replace('.', '')
      .trim();
  }

  query = normalize(query);

  let results: any = [];
  let autocomplete = '';
  let didYouMean = '';
  let didYouMeanSimilarity = 3;

  const exactMatches: any = {};
  const termMatches: any = {};

  let scored: any = {};

  const fields = ['name', 'category_names'];

  // tslint:disable-next-line: forin
  outer: for (const t in mapPlaces) {
    const tenant = mapPlaces[t];

    // tslint:disable-next-line: forin
    midder: for (const f in fields) {
      const field = fields[f];
      const fieldValue = normalize(tenant[field]);

      exactMatches[field] = exactMatches[field] || [];
      termMatches[field] = termMatches[field] || [];

      if (fieldValue.startsWith(query)) {
        exactMatches[field].push(tenant);
        if (autocomplete === '') { autocomplete = fieldValue.substring(query.length); }
        continue outer;
      }

      const terms = fieldValue.split(/[\s,]+/);
      const queryTerms = query.split(' ');

      // tslint:disable-next-line: forin
      for (let q in queryTerms) {
        q = queryTerms[q];

        inner: for (const i in terms) {
          if (!terms[i]) { continue; }
          const term = terms[i];
          const gid = tenant._id;

          const score = damerau_levenshtein(term, q);

          if (score < didYouMeanSimilarity && score > 0) {
            didYouMean = term;
            didYouMeanSimilarity = score;
          }

          if (term.startsWith(q)) {
            termMatches[field].push(tenant);
            if (scored[gid]) { delete scored[gid]; }
            continue outer;
          }

          if (score < 3) {
            scored[gid] = scored[gid] || {};
            scored[gid][field] = scored[gid][field] || null;
            if (!scored[gid][field] || score < scored[gid][field]) { scored[gid][field] = score; }
          }
        }
      }
    }
  }

  scored = Object.keys(scored).map((item) => {
    // reduce overall score to lowest score of each field
    return {
      gid: item,
      score: fields.reduce((acc, field) => {
        return (acc < scored[item][field] ? acc : scored[item][field]);
      }, 4),
    };
  }).filter((item) => {
    return item.score < 2;
  }).sort((a, b) => {
    // sort scores lowest to highest (low is a better match)
    return a.score - b.score;
  }).map((item) => {
    // map results to tenants
    return mapPlaces.find((place: any) => place._id === item.gid);
  }).filter((item) => item !== undefined);

  // tslint:disable-next-line: forin
  for (const f in fields) {
    const field = fields[f];
    exactMatches[field] = exactMatches[field].concat(termMatches[field]);
  }

  results = exactMatches.name.concat(scored);

  logTouch({
    device_name: (window as any).initParam,
    touch_type: 'Search Item',
    search_item: query,
    num_search_results: results.length,
  });

  return {
    autocomplete,
    results,
    didYouMean,
  };
};

export const getNavNodeFromId = (navNodes: any[], id: string) => {
  return navNodes.find((navNode) => navNode.properties.id === id);
};

export const getRoute = (from: string, to: string, isAda: boolean = false) => {
  const vuexStore = (window as any).EIDChromeLibrary ?
  (window as any).EIDChromeLibrary.vuexStore :
  (window as any).vuexStore;

  const navNodes = vuexStore.getters['appData/NAV_NODES'];
  const elevators = vuexStore.getters['appData/ELEVATORS'];
  const escalators = vuexStore.getters['appData/ESCALATORS'];
  const routingGraph = vuexStore.getters['appData/ROUTING_GRAPH'];

  const fromNode = getNavNodeFromId(navNodes, from);
  const toNode = getNavNodeFromId(navNodes, to);

  if (from === to) {
    const point = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: fromNode.geometry.coordinates,
      },
      properties: {
        level: fromNode.properties.level,
        id: from,
      },
    };

    return {
      cost: 0.000000001,
      initial_direction: 0,
      path: [point],
      segments: [{
        type: 'Feature',
        properties: {
          type: 'path',
          start: 'route_start',
          end: 'route_end',
          level: fromNode.properties.level,
        },
        geometry: {
          type: 'LineString',
          coordinates: [fromNode.geometry.coordinates],
        },
      }],
    };
  }

  const escalatorsAllowedLevels = [
    '5d76a8ba14dfb90011df0969', // Lower Level
    '5d76b6b314dfb90011df096a', // L1
    '5d76b761ff4bdb00110b0a09', // L2
  ];

  const toAvoid: any = (fromNode.properties.level === toNode.properties.level) ?
    escalators.concat(elevators) :
    (isAda || !escalatorsAllowedLevels.includes(toNode.properties.level)) ?
      escalators :
      // tslint:disable-next-line: max-line-length
      (escalatorsAllowedLevels.includes(fromNode.properties.level) && escalatorsAllowedLevels.includes(toNode.properties.level)) ?
        elevators :
        [];

  [from, to].forEach((node) => {
    const index = toAvoid.indexOf(node);

    if (index > -1) {
      toAvoid.splice(index, 1);
    }
  });

  let route: any = {
    path: null,
  };

  try {
    route = routingGraph.path(from, to, { cost: true, avoid: toAvoid });
  } catch (e) {
    route = { path: null };
  }

  if (route.path === null) {
    try {
      route = routingGraph.path(from, to, { cost: true, avoid: [] });
    } catch (e) {
      route = { path: null };
    }
  }

  if (route.path !== null) {
    route.path = route.path.map((nodeId: any) => {
      const node = getNavNodeFromId(navNodes, nodeId);

      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: node.geometry.coordinates,
        },
        properties: {
          level: node.properties.level,
          id: nodeId,
        },
      };
    });

    route.segments = route.path.reduce((acc: any, point: any, index: any, arr: any) => {
      const currentSegment = acc[acc.length - 1];

      if (currentSegment === undefined || currentSegment.properties.level !== point.properties.level) {
        let pointType = index === 0 ? 'route_start' :
          elevators.includes(point.properties.id) ? 'elevator' :
            escalators.includes(point.properties.id) ? 'escalator' : '';

        if (currentSegment !== undefined) {
          // It is not the first segment, and we've left the previous one, so we must add properties.end.

          if (pointType === '') {
            // The current point isn't an escalator or elevator, so check if the previous point was.
            const previousPoint = arr[index - 1];

            pointType = elevators.includes(previousPoint.properties.id) ? 'elevator' :
              escalators.includes(previousPoint.properties.id) ? 'escalator' : '';
          }

          currentSegment.properties.end = pointType;
        }

        // This point is the beginning of a new level.
        acc.push({
          type: 'Feature',
          properties: {
            type: 'path',
            start: pointType,
            end: '',
            level: point.properties.level,
          },
          geometry: {
            type: 'LineString',
            coordinates: [point.geometry.coordinates],
          },
        });

      } else {
        // The current point is on the same level as the previous point.
        currentSegment.geometry.coordinates.push(point.geometry.coordinates);
      }

      if (index === arr.length - 1) {
        // It is the last point in the route.
        currentSegment.properties.end = 'route_end';
      }

      return acc;
    }, []);

    // Filter out segments that aren't the first or last, and have only one node.
    route.segments = route.segments.filter((segment: any, index: any, array: any) => {
      return index === 0 || (index + 1) === array.length || segment.geometry.coordinates.length > 1;
    });

    // Calculate the direction of each route segment.
    route.segments.forEach((segment: any) => {
      const coords = segment.geometry.coordinates;
      const start = coords[0];
      let end = coords[coords.length - 1];
      if (coords.length > 5) {
        end = coords[4];
      }

      const radians = Math.atan2(degToRad(end[0]) - degToRad(start[0]), degToRad(end[1]) - degToRad(start[1]));
      segment.properties.direction = radToDeg(radians);

      // round to nearest 45
      segment.properties.direction = 45 * Math.round(segment.properties.direction / 45);
      if (segment.properties.direction < 0) {
        segment.properties.direction += 360;
      }
    });

    route.initial_direction = route.segments[0].properties.direction;

    const floorChanges = route.segments.length - 1;
    route.cost -= floorChanges * 0.001; // because of the extra large penalty for floor changes,
    // we need to reduce it in the actual cost.

    route.steps = Math.round(route.cost * 300000);

    route.time = Math.round(route.cost * 1000);
    if (route.time < 1) {
      route.time = 1;
    }
  }

  return route;
};
