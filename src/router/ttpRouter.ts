import Vue from 'vue';
import Router from 'vue-router';
import TtpWayfinder from '@/views/TtpWayfinder.vue';

Vue.use(Router);

const baseRoutes: any = [
  {
    path: '/wayfinder/:from/:to/:facingDirection/:ada',
    name: 'wayfinder',
    component: TtpWayfinder,
    props: true,
  },
  {
    path: '/wayfinder/:from/:to/:facingDirection/:ada/:toNav',
    name: 'wayfinder-specific',
    component: TtpWayfinder,
    props: true,
  },
  {
    path: '*',
    redirect: {
      name: 'wayfinder',
    },
  },
];

const router = new Router({
  routes: baseRoutes,
});

export default router;
