import Categories from '@/views/Categories.vue';
import CategoryDesigners from '@/views/CategoryDesigners.vue';
import DesignerCategories from '@/views/DesignerCategories.vue';
import Designers from '@/views/Designers.vue';
import Dining from '@/views/Dining.vue';
import Floors from '@/views/Floors.vue';
import Home from '@/views/Home.vue';
import NearestRestrooms from '@/views/NearestRestrooms.vue';
import Restrooms from '@/views/Restrooms.vue';
import SaksAtYourService from '@/views/SaksAtYourService.vue';
import Search from '@/views/Search.vue';
import ServiceDetail from '@/views/ServiceDetail.vue';
import Wayfinder from '@/views/Wayfinder.vue';
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const baseRoutes: any = [
  {
    path: '/home',
    name: 'home',
    component: Home,
  },
  {
    path: '/designers',
    name: 'designers',
    component: Designers,
  },
  {
    path: '/designers/:designerName',
    name: `designer-categories`,
    component: DesignerCategories,
    props: true,
  },
  {
    path: '/categories',
    name: 'categories',
    component: Categories,
  },
  {
    path: '/categories/:categoryId',
    name: 'category-designers',
    component: CategoryDesigners,
    props: true,
  },
  {
    path: '/dining',
    name: 'dining',
    component: Dining,
  },
  {
    path: '/saks-at-your-service',
    name: 'saks-at-your-service',
    component: SaksAtYourService,
  },
  {
    path: '/saks-at-your-service/:locationId',
    name: 'saks-at-your-service-location',
    component: ServiceDetail,
    props: true,
  },
  {
    path: '/search',
    name: 'search',
    component: Search,
  },
  {
    path: '/search/:query',
    name: 'search query',
    component: Search,
    props: true,
  },
  {
    path: '/floors',
    name: 'floors',
    component: Floors,
  },
  {
    path: '/floors/:floorId',
    name: 'floors-floor',
    component: Floors,
    props: true,
  },
  {
    path: '/wayfinder/:to',
    name: 'wayfinder',
    component: Wayfinder,
    props: true,
  },
  {
    path: '/wayfinder/:to/:toNav',
    name: 'wayfinder-specific',
    component: Wayfinder,
    props: true,
  },
  {
    path: '/restrooms',
    name: 'restrooms',
    component: Restrooms,
  },
  {
    path: '/nearest-restrooms',
    name: 'nearest-restrooms',
    component: NearestRestrooms,
  },
  {
    path: '*',
    redirect: '/home',
  },
];

const router = new Router({
  mode: 'history',
  routes: baseRoutes,
});

export default router;
