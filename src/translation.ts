export const phrase = (phraseToTranslate: string, language: string) => {
  if (phraseToTranslate in langLibrary && language in langLibrary[phraseToTranslate]) {
    return langLibrary[phraseToTranslate][language];
  } else {
    return '';
  }
};

const langLibrary: any = {
  language: {
    en: {
      native: 'English',
    },
    zh: {
      native: '普通话',
      en: 'Mandarin',
    },
  },
  back: {
    en: 'back',
    zh: '回去',
  },
  home: {
    en: 'home',
    zh: '回家',
  },
  continue: {
    en: 'Continue',
    zh: '继续',
  },
  start_over: {
    en: 'Start Over',
    zh: '重来',
  },
  search: {
    en: 'Search',
    zh: '搜索',
  },
  categories: {
    en: 'Categories',
    zh: '分类',
  },
  designers: {
    en: 'Designers',
    zh: '设计师',
  },
  most_searched_for: {
    en: 'Most Searched For',
    zh: '搜索最多的',
  },
  restrooms: {
    en: 'restrooms',
    zh: '洗手间',
  },
  nearest_restroom: {
    en: 'Nearest Restroom',
    zh: '最近的洗手间',
  },
  customer_service: {
    en: 'Customer Service',
    zh: '客户服务',
  },
  sakscom_pickup: {
    en: 'Saks.com Pickup',
    zh: 'Saks.com 取货',
  },
  services__amenities: {
    en: 'Services & Amenities',
    zh: '服务与设施',
  },
  saks_at_your_service: {
    en: 'Saks at Your Service',
    zh: 'Saks 乐意效劳',
  },
  floors: {
    en: 'floors',
    zh: '地板',
  },
  text_me_directions: {
    en: 'Text Me Directions',
    zh: '给我发短信',
  },
  directions: {
    en: 'directions',
    zh: '指示',
  },
  atm: {
    en: 'ATM',
    zh: '自动取款机',
  },
  elevator: {
    en: 'elevator',
    zh: '电梯',
  },
  escalator: {
    en: 'escalator',
    zh: '自动扶梯',
  },
  this_way: {
    en: 'this way',
    zh: '这条路',
  },
  clear: {
    en: 'clear',
    zh: '明确',
  },
  space: {
    en: 'space',
    zh: '空间',
  },
  you_are_here: {
    en: 'You Are Here',
    zh: '你在这里',
  },
  dining: {
    en: 'dining',
    zh: '用餐',
  },
  more_info: {
    en: 'More Info',
    zh: '更多信息',
  },
  go: {
    en: 'Go',
    zh: '走',
  },
  destination_is_on: {
    en: 'Destination is on',
    zh: '目的地已开启',
  },
  minutes_to_destination: {
    en: 'minutes to destination',
    zh: '分钟到目的地',
  },
  current_level: {
    en: 'Current Level',
    zh: '现在的水平',
  },
  lower_level: {
    en: 'Lower Level',
    zh: '低等级',
  },
  level: {
    en: 'Level',
    zh: '水平',
  },
  closes_at: {
    en: 'Closes at',
    zh: '在关闭',
  },
  closed: {
    en: 'CLOSED',
    zh: '关闭',
  },
};
