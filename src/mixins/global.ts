import { addBaseVoiceCommands, scrollHandler, removeVoiceCommand, addVoiceCommands } from '@/helpers';
import encoding from 'encoding';
import { JWT } from 'google-auth-library';
import { Component, Vue } from 'vue-property-decorator';
import googleServiceAccountKey from '../google-private-key.json';

const googleJWTClient = new JWT(
  googleServiceAccountKey.client_email,
  undefined,
  googleServiceAccountKey.private_key,
  [
    'https://www.googleapis.com/auth/cloud-platform',
    'https://www.googleapis.com/auth/cloud-translation',
  ],
);

@Component
export default class GlobalMixIn extends Vue {
  // Methods

  public scrollHandler = scrollHandler;
  public resetVoiceCommands = addBaseVoiceCommands;
  public removeVoiceCommand = removeVoiceCommand;
  public addVoiceCommands = addVoiceCommands;

  public async getDynamicTranslation(text: string): Promise<string> {
    if (this.currentLanguage === 'en') {
      return text;
    } else {
      const phraseToken = text.replace(/ /g, '_').replace(/[^\w]/gi, '').toLowerCase();
      const language = this.currentLanguage;

      const existingTranslatedPhrase = this.$store.getters['appData/TRANSLATED_PHRASE']({ phraseToken, language });

      if (existingTranslatedPhrase !== null) {
        return encoding.convert(existingTranslatedPhrase, 'latin1', 'utf-8');
      } else if (text.trim() !== '') {
        try {
          const requestOptions: any = {
            url: 'https://translation.googleapis.com/language/translate/v2',
            method: 'POST',
            data: {
              q: text,
              source: 'en',
              target: language,
              format: 'text',
            },
          };

          const translatedPhraseRequest: any = await googleJWTClient.request(requestOptions);

          const translatedPhraseData = translatedPhraseRequest.data.data.translations;

          if (translatedPhraseData[0] && translatedPhraseData[0].translatedText) {
            this.$store.commit('appData/SET_TRANSLATED_PHRASE', {
              phraseToken,
              language,
              translatedPhrase: translatedPhraseData[0].translatedText,
            });

            return translatedPhraseData[0].translatedText;
          } else {
            return text;
          }
        } catch (e) {
          return text;
        }
      } else {
        return text;
      }
    }
  }

  // Computed Properties

  get currentLanguage() {
    return this.$store.getters['appData/CURRENT_LANGUAGE'];
  }

  get online() {
    return this.$store.getters[`${process.env.VUE_APP_FALLBACK_DEVICE_ID}/ONLINE_STATUS`];
  }

  get ada() {
    return this.$store.getters['appData/ADA'];
  }
}
