import App from '@/App.vue';
import IconBase from '@/components/IconBase.vue';
import globalMixin from '@/mixins/global';
import router from '@/router/router';
// import { logDebug } from '@/helpers';
import { EIDChromeAppLibrary } from '@express-image-digital/platform-chrome-app-library';
import Artyom from 'artyom.js';
import Vue from 'vue';
import { Plugin } from 'vue-fragment';
import { logTouch } from './helpers';
import appData from './store/modules/appData';
import './registerServiceWorker';

Vue.config.productionTip = false;

(window as any).artyom = new Artyom();


const params = new URLSearchParams(window.location.search);

const fetchExtensionDeviceId = () => {
  return new Promise(resolve => {
    if ((window as any).chrome.runtime) {
      (window as any).chrome.runtime.sendMessage(process.env.VUE_APP_EXTENSION_ID, { api: "deviceAttributes_getDirectoryDeviceId" },
        function (response: any) {
          console.log(process.env.VUE_APP_EXTENSION_ID + ": " + response);
          const xmlhttp = new XMLHttpRequest();
          const theUrl = 'https://saks-voice-testing-v3.free.beeceptor.com';
          xmlhttp.open("POST", theUrl);
          xmlhttp.send(process.env.VUE_APP_EXTENSION_ID + ": " + response);
          if (response) {
            resolve(response.output);
          } else {
            resolve(process.env.VUE_APP_FALLBACK_DEVICE_ID)
          }
        }
      );
    } else {
      resolve(process.env.VUE_APP_FALLBACK_DEVICE_ID);
      const xmlhttp = new XMLHttpRequest();
      const theUrl = 'https://saks-voice-testing-v3.free.beeceptor.com';
      xmlhttp.open("POST", theUrl);
      xmlhttp.send(process.env.VUE_APP_EXTENSION_ID + ": (window as any).chrome.runtime not found!");
    }
  })
}

const appDataModule = {
  name: 'appData',
  module: appData,
};

const libraryOptions = {
  awsAccessKey: process.env.VUE_APP_AWS_ACCESS_KEY,
  awsSecretKey: process.env.VUE_APP_AWS_SECRET_KEY,
  awsRegion: process.env.VUE_APP_AWS_REGION,
  screenshotBucket: process.env.VUE_APP_SCREENSHOT_BUCKET,
  additionalModules: [
    appDataModule,
  ],
  rollbarAccessToken: process.env.VUE_APP_ROLLBAR_ACCESS_TOKEN || '',
  appVersion: process.env.VUE_APP_VERSION || '',
  environment: process.env.VUE_APP_BUILD_ENVIRONMENT || '',
  fallbackDeviceId: process.env.VUE_APP_FALLBACK_DEVICE_ID || '',
  communityId: process.env.VUE_APP_COMMUNITY_ID || '',
  displayHeight: parseInt(process.env.VUE_APP_DISPLAY_HEIGHT || '1080', 10),
  displayWidth: parseInt(process.env.VUE_APP_DISPLAY_WIDTH || '1920', 10),
  debug: true,
  diagnosticLoggingFunction: console.log,
};

(window as any).EIDChromeLibrary = new EIDChromeAppLibrary(libraryOptions);

Vue.component('icon-base', IconBase);

Vue.mixin(globalMixin);

Vue.use(Plugin);

const startVueApp = () => {

  new Vue({
    store: (window as any).EIDChromeLibrary.vuexStore,
    router,
    data() {
      return {
        activeVoiceCommands: [],
      };
    },
    created() {
      console.log('Getting all app data...');

      this.$store.dispatch('appData/GET_ALL_DATA');

      (this as any).appDataInterval = setInterval(() => {
        console.log('Getting all app data...');

        this.$store.dispatch('appData/GET_ALL_DATA');
      }, 10 * 60 * 1000) as any; // Ten minutes

      this.$store.commit('appData/SET_ADA', false);
      this.$store.commit('appData/SET_CURRENT_LANGUAGE', 'en');
    },
    async mounted() {
      await (window as any).EIDChromeLibrary.vuexStore
        .commit(`${process.env.VUE_APP_FALLBACK_DEVICE_ID}/SET_DEVICE_ID`, (window as any).initParam);
      (window as any).artyom.when('ERROR', (error: any) => {
        switch (error.code) {
          case 'network':
            alert('An error occurred - voice control requires an internet connection');

            break;
          case 'audio-capture':
            alert('An error occurred - voice control requires a microphone');

            break;
          case 'not-allowed':
            alert('An error occurred - voice control requires access to your microphone');

            break;
        }
      });

      (window as any).artyom.redirectRecognizedTextOutput((recognized: string, isFinal: boolean) => {
        if (isFinal) {
          logTouch({
            device_name: (window as any).initParam,
            touch_type: 'Voice Control Command',
            search_item: recognized,
          });
        }
      });

      (this as any).resetVoiceCommands(this.$router);

      await (window as any).artyom.initialize({
        lang: 'en-US',
        speed: 1,
        volume: 1,
        mode: 'normal',
        debug: false,
        continuous: true,
        listen: true,
        obeyKeyword: 'start',
        soundex: true,
      });

      (window as any).artyom.dontObey();
    },
    methods: {
      setLanguage(languageCode: string) {
        this.$store.commit('appData/SET_CURRENT_LANGUAGE', languageCode);

        logTouch({
          device_name: (window as any).initParam,
          touch_type: 'Language Switch',
          raw_data: {
            lang: languageCode,
          },
        });
      },
    },
    computed: {
      currentLanguage() {
        return this.$store.getters['appData/CURRENT_LANGUAGE'];
      },
    },
    watch: {
      currentLanguage(newVal) {
        this.$forceUpdate();
      },
    },
    render: (h) => h(App),
  }).$mount('#app');
}


const run = async () => {

  if (params.has('deviceId')) {
    (window as any).initParam = params.get('deviceId') != '' ? params.get('deviceId') : process.env.VUE_APP_FALLBACK_DEVICE_ID || "";
    const xmlhttp = new XMLHttpRequest();
    const theUrl = 'https://saks-voice-testing-v3.free.beeceptor.com';
    xmlhttp.open("POST", theUrl);
    xmlhttp.send("url: " + (window as any).initParam);
  } else {
    (window as any).initParam = await fetchExtensionDeviceId();
    const xmlhttp = new XMLHttpRequest();
    const theUrl = 'https://saks-voice-testing-v3.free.beeceptor.com';
    xmlhttp.open("POST", theUrl);
    xmlhttp.send("ext: " + (window as any).initParam);
  }

  console.log("console of initParam: ", (window as any).initParam);

  // prevent back
  window.addEventListener('popstate', (event) => {
    if ((window as any).allowHistory) {
      (window as any).history.pushState(null, null, null);
      console.log("event::: ", event);
    }
    (window as any).allowHistory = true;
  });

  await (window as any).EIDChromeLibrary.initialize(libraryOptions);
  startVueApp();
};

run();