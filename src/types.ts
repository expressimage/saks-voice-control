export interface AppData {
  pois: POI[];
  elevators: string[];
  escalators: string[];
  stairs: string[];
  navNodes: any;
  navLinks: any;
  levels: AppDataLevel[];
  buildingFeatures: BuildingFeature[];
  levelFeatures: LevelFeature[];
  obstructionFeatures: ObstructionFeature[];
  spaceFeatures: SpaceFeature[];
  connectionFeatures: ConnectionFeature[];
  kiosks: POI[];
  translatedPhrases: TranslatedPhrase;
  categories: AppDataCategory[];
  storeHours: StoreHoursEntry[];
  routingGraph: any;
  loaded: boolean;
  ada: boolean;
  currentLanguage: 'en' | 'zh' | 'es' | 'fr' | 'pt' | 'it' | 'de' | 'ar' | 'ru' | 'tl';
}

export interface TranslatedPhrase {
  [x: string]: TranslatedPhraseObject;
}

export interface TranslatedPhraseObject {
  [x: string]: string;
}

export interface AppDataLevel {
  id: string;
  name: string;
  number: string;
  abbreviation: string;
}

export interface AppDataCategory {
  id: string;
  name: string;
  // icon?: string | null;
  // iconDataURL: string | null;
}

export interface POI {
  id: string;
  name: string;
  type: string;
  levels: AppDataLevel[];
  spaces: string[];
  categories: string[];
  navNodes?: POINavNode[];
  mediaDataURL?: string;
  description?: string;
  // logo?: string | null;
  // logoDataURL: string | null;
  // picture?: string | null;
  // pictureDataURL: string | null;
}

export interface POINavNode {
  space: string;
  navNode: string;
}

export interface MapPlaceType {
  name: string;
}

export interface DeviceData {
  api: any;
  communityId: string;
  deviceId: string;
  availMemory: number;
  cpuUsage: number;
  displayHeight: number;
  name: string;
  appVersion: string;
  appVersionName: string;
  osVersion: string;
  comment: string;
  settings: object;
  remoteAction: string;
  onlineStatus: boolean;
}

export interface FlatfileData {
  mapping_data: MappingData;
  storeHours: StoreHoursEntry[];
  manualImages: ManualImageEntry[];
  manualDescriptions: ManualDescriptionEntry[];
  translations: TranslationEntry;
}

export interface TranslationEntry {
  [x: string]: TranslationLanguageEntry;
}

export interface TranslationLanguageEntry {
  [x: string]: string;
}

export interface ManualImageEntry {
  id: string;
  imageURL: string;
  mediaDataURL?: string | null;
}

export interface ManualDescriptionEntry {
  id: string;
  description: string;
}

export interface StoreHoursEntry {
  closing_time: string;
  dates: string[];
}

export interface MappingData {
  building: Building;
  level: Level;
  space: Space;
  node: Node;
  obstruction: Obstruction;
  category: Category;
  location: Location;
  connection: Connection;
  manifest: any;
}

export interface FeatureGeometry {
  type: 'Point' | 'LineString' | 'Polygon';
  coordinates: number[] | number[][][];
}

// BUILDINGS

export interface Building {
  type: 'FeatureCollection';
  features: BuildingFeature[];
}

export interface BuildingFeature {
  type: 'Feature';
  geometry: BuildingFeatureGeometry;
  properties: BuildingProperties;
}

export interface BuildingFeatureGeometry {
  type: 'Polygon';
  coordinates: number[][][];
}

export interface BuildingProperties {
  id: string;
  venue: string;
  name: string;
}

// LEVELS

export interface Level {
  [x: string]: LevelObject;
}

export interface LevelObject {
  type: 'FeatureCollection';
  features: LevelFeature[];
}

export interface LevelFeature {
  type: 'Feature';
  geometry: LevelFeatureGeometry;
  properties: LevelProperties;
}

export interface LevelFeatureGeometry {
  type: 'Polygon';
  coordinates: number[][][];
}

export interface LevelProperties {
  id: string;
  building: string;
  name: string;
  abbreviation: string;
  elevation: string;
}

// SPACES

export interface Space {
  [x: string]: SpaceObject;
}

export interface SpaceObject {
  type: 'FeatureCollection';
  features: SpaceFeature[];
}

export interface SpaceFeature {
  type: 'Feature';
  geometry: SpaceFeatureGeometry;
  properties: SpaceProperties;
}

export interface SpaceFeatureGeometry {
  type: 'Polygon';
  coordinates: number[][][];
}

export interface SpaceProperties {
  id: string;
  level: string;
  parent: string | null;
  externalId: string | null;
  layer: string;
  color: string;
  height: number;
}

// NODES

export interface Node {
  [x: string]: NodeObject;
}

export interface NodeObject {
  type: 'FeatureCollection';
  features: NodeFeature[];
}

export interface NodeFeature {
  type: 'Feature';
  geometry: NodeFeatureGeometry;
  properties: NodeProperties;
}

export interface NodeFeatureGeometry {
  type: 'Point';
  coordinates: number[];
}

export interface NodeProperties {
  id: string;
  level: string;
  neighbors: string[];
  weight: number;
}

// OBSTRUCTIONS

export interface Obstruction {
  [x: string]: ObstructionObject;
}

export interface ObstructionObject {
  type: 'FeatureCollection';
  features: ObstructionFeature[];
}

export interface ObstructionFeature {
  type: 'Feature';
  geometry: ObstructionFeatureGeometry;
  properties: ObstructionProperties;
}

export interface ObstructionFeatureGeometry {
  type: 'Polygon';
  coordinates: number[][][];
}

export interface ObstructionProperties {
  id: string;
  level: string;
  parent: string | null;
  externalId: string | null;
}

// CONNECTIONS

export interface Connection {
  [x: string]: ConnectionObject;
}

export interface ConnectionObject {
  type: 'FeatureCollection';
  features: ConnectionFeature[];
}

export interface ConnectionFeature {
  type: 'Feature';
  geometry: ConnectionFeatureGeometry;
  properties: ConnectionProperties;
}

export interface ConnectionFeatureGeometry {
  type: 'Point';
  coordinates: number[];
}

export interface ConnectionProperties {
  id: string;
  level: string;
  destinations: string[];
  name: number;
  type: 'escalator' | 'elevator' | 'stairs';
}

// CATEGORIES

export interface Category {
  type: 'FeatureCollection';
  features: CategoryFeature[];
}

export interface CategoryFeature {
  type: 'Feature';
  geometry: null;
  properties: CategoryProperties;
}

export interface CategoryProperties {
  id: string;
  name: string;
  picture: MappedInImage;
}

export interface MappedInImage {
  original: string;
  xlarge: string;
  xsmall: string;
  medium: string;
  xxlarge: string;
  xxsmall: string;
  large: string;
  small: string;
}

// LOCATIONS

export interface Location {
  type: 'FeatureCollection';
  features: LocationFeature[];
}

export interface LocationFeature {
  type: 'Feature';
  geometry: null;
  properties: LocationProperties;
}

export interface LocationProperties {
  id: string;
  spaces: LocationSpace[];
  type: string;
  hours: null;
  categories: string[];
  name: string;
  externalId: string;
  states: string[];
  logo: MappedInImage | null;
  picture: MappedInImage | null;
  address: string | null;
  phone: string | null;
  email: string | null;
  social: string | null;
}

export interface LocationSpace {
  id: string;
  map: string;
  node?: string;
}
