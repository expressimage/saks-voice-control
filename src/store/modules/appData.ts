import nodeDijkstra from 'node-dijkstra';
import axios from 'axios';
import * as types from '@/types';
import { logError, distanceBetween, logTouch, insideRobust } from '@/helpers';
import { DownloadManager } from '@express-image-digital/platform-chrome-app-library';

const downloadManager = new DownloadManager({ debug: false });

const appData: types.AppData = {
  pois: [],
  elevators: [],
  escalators: [],
  stairs: [],
  navNodes: {},
  navLinks: {},
  kiosks: [],
  categories: [],
  levels: [],
  buildingFeatures: [],
  levelFeatures: [],
  obstructionFeatures: [],
  spaceFeatures: [],
  connectionFeatures: [],
  storeHours: [],
  translatedPhrases: {},
  routingGraph: null,
  loaded: false,
  ada: false,
  currentLanguage: 'en',
};

export default {
  namespaced: true,
  state: appData,
  getters: {
    POIS: (state: types.AppData) => state.pois,
    ELEVATORS: (state: types.AppData) => state.elevators,
    ESCALATORS: (state: types.AppData) => state.escalators,
    STAIRS: (state: types.AppData) => state.stairs,
    NAV_NODES: (state: types.AppData) => state.navNodes,
    NAV_LINKS: (state: types.AppData) => state.navLinks,
    KIOSKS: (state: types.AppData) => state.kiosks,
    TRANSLATED_PHRASE : (state: types.AppData) => (payload: any) =>
      state.translatedPhrases[payload.phraseToken] && state.translatedPhrases[payload.phraseToken][payload.language] ?
        state.translatedPhrases[payload.phraseToken][payload.language] :
        null,
    TRANSLATED_PHRASES: (state: types.AppData) => state.translatedPhrases,
    CATEGORIES: (state: types.AppData) => state.categories,
    LEVELS: (state: types.AppData) => state.levels,
    BUILDING_FEATURES: (state: types.AppData) => state.buildingFeatures,
    LEVEL_FEATURES: (state: types.AppData) => state.levelFeatures,
    OBSTRUCTION_FEATURES: (state: types.AppData) => state.obstructionFeatures,
    SPACE_FEATURES: (state: types.AppData) => state.spaceFeatures,
    CONNECTION_FEATURES: (state: types.AppData) => state.connectionFeatures,
    ROUTING_GRAPH: (state: types.AppData) => state.routingGraph,
    STORE_HOURS: (state: types.AppData) => state.storeHours,
    LOADED : (state: types.AppData) => state.loaded,
    ADA : (state: types.AppData) => state.ada,
    CURRENT_LANGUAGE : (state: types.AppData) => state.currentLanguage,
  },
  mutations: {
    RESET_APP_DATA: (state: types.AppData) => {
      state = appData;
    },
    SET_POIS: (state: types.AppData, payload: types.POI[]) => {
      state.pois = payload;
    },
    SET_ELEVATORS: (state: types.AppData, payload: string[]) => {
      state.elevators = payload;
    },
    SET_ESCALATORS: (state: types.AppData, payload: string[]) => {
      state.escalators = payload;
    },
    SET_STAIRS: (state: types.AppData, payload: string[]) => {
      state.stairs = payload;
    },
    SET_NAV_NODES: (state: types.AppData, payload: any) => {
      state.navNodes = payload;
    },
    SET_NAV_LINKS: (state: types.AppData, payload: any) => {
      state.navLinks = payload;
    },
    SET_KIOSKS: (state: types.AppData, payload: types.POI[]) => {
      state.kiosks = payload;
    },
    SET_CATEGORIES: (state: types.AppData, payload: types.AppDataCategory[]) => {
      state.categories = payload;
    },
    SET_LEVELS: (state: types.AppData, payload: types.AppDataLevel[]) => {
      state.levels = payload;
    },
    SET_BUILDING_FEATURES: (state: types.AppData, payload: types.BuildingFeature[]) => {
      state.buildingFeatures = payload;
    },
    SET_LEVEL_FEATURES: (state: types.AppData, payload: types.LevelFeature[]) => {
      state.levelFeatures = payload;
    },
    SET_OBSTRUCTION_FEATURES: (state: types.AppData, payload: types.ObstructionFeature[]) => {
      state.obstructionFeatures = payload;
    },
    SET_SPACE_FEATURES: (state: types.AppData, payload: types.SpaceFeature[]) => {
      state.spaceFeatures = payload;
    },
    SET_CONNECTION_FEATURES: (state: types.AppData, payload: types.ConnectionFeature[]) => {
      state.connectionFeatures = payload;
    },
    SET_ROUTING_GRAPH: (state: types.AppData, payload: any) => {
      state.routingGraph = payload;
    },
    SET_STORE_HOURS: (state: types.AppData, payload: types.StoreHoursEntry[]) => {
      state.storeHours = payload;
    },
    SET_TRANSLATED_PHRASE: (state: types.AppData, payload: any) => {
      const phraseToken = payload.phraseToken;
      const language = payload.language;
      const translatedPhrase = payload.translatedPhrase;

      if (state.translatedPhrases[phraseToken]) {
        state.translatedPhrases[phraseToken][language] = translatedPhrase;
      } else {
        state.translatedPhrases[phraseToken] = {};

        state.translatedPhrases[phraseToken][language] = translatedPhrase;
      }
    },
    SET_LOADED: (state: types.AppData, payload: boolean) => {
      state.loaded = payload;
    },
    SET_ADA: (state: types.AppData, payload: boolean) => {
      state.ada = payload;
    },
    SET_CURRENT_LANGUAGE: ( state: types.AppData,
                            payload: 'en' | 'zh' | 'es' | 'fr' | 'pt' | 'it' | 'de' | 'ar' | 'ru' | 'tl') => {
      state.currentLanguage = payload;

      if (['ar'].includes(payload)) {
        document.body.style.direction = 'rtl';
      } else {
        document.body.style.direction = 'ltr';
      }
    },
  },
  actions: {
    GET_ALL_DATA: async ({ state, dispatch, commit }: any) => {
      await dispatch('GET_APP_DATA');

      await commit('SET_LOADED', true);
    },
    TOGGLE_ADA: async ({ getters, commit }: any) => {
      const currentADA = getters.ADA;

      const newADA = !currentADA;

      document.body.classList[newADA ? 'add' : 'remove']('ada');

      commit('SET_ADA', newADA);

      logTouch({
        device_name: (window as any).initParam,
        touch_type: 'ADA Toggle',
        raw_data: {
          ada_mode: newADA,
        },
      });
    },
    GET_APP_DATA: async ({ getters, commit }: any) => {
      try {
        const fileData = await axios.get(process.env.VUE_APP_FLATFILE_URL || '');

        const flatfileJSON: types.FlatfileData = fileData.data;

        const storeHours = flatfileJSON.storeHours;
        const manualDescriptions = flatfileJSON.manualDescriptions;
        const manualImages = flatfileJSON.manualImages;
        const mappingData = flatfileJSON.mapping_data;
        const translations = flatfileJSON.translations;

        const translatedPhrases = getters.TRANSLATED_PHRASES;

        await Promise.all((Object.keys(translations).map(async (phraseToken) => {
          await Promise.all((Object.keys(translations[phraseToken]).map(async (language) => {
            if (!(translatedPhrases[phraseToken] && translatedPhrases[phraseToken][language])) {
              await commit('SET_TRANSLATED_PHRASE', {
                phraseToken,
                language,
                translatedPhrase: translations[phraseToken][language],
              });
            }
          })));
        })));

        const building = mappingData.building;
        const levels = mappingData.level;
        const spaces = mappingData.space;
        const nodes = mappingData.node;
        const obstructions = mappingData.obstruction;
        const connections = mappingData.connection;
        const categories = mappingData.category;
        const locations = mappingData.location;

        // Find kiosks in data

        const kioskLocation = locations.features.find((feature) => feature.properties.name === 'Directory');

        const kiosks = kioskLocation || [];

        // Get categories data

        const appDataCategories: types.AppDataCategory[] = categories.features.map((category) => {
          return {
            id: category.properties.id,
            name: category.properties.name,
            // icon: category.properties.picture ? category.properties.picture.original : null,
            // iconDataURL: null,
          };
        });

        // Get level data

        const appDataLevels: types.AppDataLevel[] = Object.keys(levels).map((level): types.AppDataLevel => {
          const levelData = levels[level].features[0].properties;

          return {
            id: levelData.id,
            name: levelData.name,
            number: levelData.elevation,
            abbreviation: levelData.abbreviation,
          };
        });

        const levelIds = Object.keys(levels);

        // Get all nav node features

        // tslint:disable-next-line: max-line-length
        const navNodes: types.NodeFeature[] = Object.keys(nodes).reduce((acc, coll) => acc.concat(nodes[coll].features), [] as types.NodeFeature[]);

        // Build nav links array

        const navLinks = navNodes.reduce((acc, feature) => {
          acc[feature.properties.id] = acc[feature.properties.id] || {};

          feature.properties.neighbors.forEach((node) => {
            acc[node] = acc[node] || {};

            const match = navNodes.find((item) => item.properties.id === node);

            if (match !== undefined && feature.properties.id !== match.properties.id) {
              // tslint:disable-next-line: max-line-length
              acc[feature.properties.id][node] = distanceBetween(feature.geometry.coordinates, match.geometry.coordinates);
            }
          });

          return acc;
        }, {} as any);

        // Set connections

        const elevators: string[] = [];
        const escalators: string[] = [];
        const stairs: string[] = [];

        levelIds.forEach((level) => {
          const features = connections[level].features;

          features.forEach((feature) => {
            if (feature.properties.type === 'elevator') {
              elevators.push(`n_${feature.properties.id}`);
            } else if (feature.properties.type === 'escalator') {
              escalators.push(`n_${feature.properties.id}`);
            } else if (feature.properties.type === 'stairs') {
              stairs.push(`n_${feature.properties.id}`);
            }

            const destination = feature.properties.destinations[0].split('-')[1];

            navLinks[`n_${feature.properties.id}`] = navLinks[`n_${feature.properties.id}`] || {};
            navLinks[`n_${destination}`] = navLinks[`n_${destination}`] || {};

            // tslint:disable-next-line: max-line-length
            navLinks[`n_${feature.properties.id}`][`n_${destination}`] = navLinks[`n_${feature.properties.id}`][`n_${destination}`] || 0.00001;
            // tslint:disable-next-line: max-line-length
            navLinks[`n_${destination}`][`n_${feature.properties.id}`] = navLinks[`n_${destination}`][`n_${feature.properties.id}`] || 0.00001;
          });
        });

        // Generate routing graph

        const routingGraph = new nodeDijkstra(navLinks);

        // Get location data

        const pois: types.POI[] = locations.features.map((location) => {
          // tslint:disable-next-line: max-line-length
          const locationLevels = appDataLevels.filter((level) => location.properties.spaces.map((space) => space.map).includes(level.id));
          const locationSpaceIds = location.properties.spaces.map((space) => space.node || space.id);
          // const locationLogo = location.properties.logo ? location.properties.logo.original : null;
          // const locationPicture = location.properties.picture ? location.properties.picture.original : null;

          return {
            id: location.properties.id,
            name: location.properties.name,
            type: location.properties.type,
            levels: locationLevels,
            spaces: locationSpaceIds,
            categories: location.properties.categories,
            // logo: locationLogo,
            // logoDataURL: null,
            // picture: locationPicture,
            // pictureDataURL: null,
          };
        });

        await commit('SET_STORE_HOURS', storeHours);

        await commit('SET_NAV_NODES', navNodes);
        await commit('SET_NAV_LINKS', navLinks);
        await commit('SET_ROUTING_GRAPH', routingGraph);

        await commit('SET_KIOSKS', kiosks);
        await commit('SET_CATEGORIES', appDataCategories);
        await commit('SET_LEVELS', appDataLevels);
        await commit('SET_STAIRS', stairs);
        await commit('SET_ELEVATORS', elevators);
        await commit('SET_ESCALATORS', escalators);

        // tslint:disable-next-line: max-line-length
        const levelFeatures = Object.keys(levels).reduce((acc, coll) => acc.concat(levels[coll].features), [] as types.LevelFeature[]);
        // tslint:disable-next-line: max-line-length
        const obstructionFeatures = Object.keys(obstructions).reduce((acc, coll) => acc.concat(obstructions[coll].features), [] as types.ObstructionFeature[]);
        // tslint:disable-next-line: max-line-length
        const spaceFeatures = Object.keys(spaces).reduce((acc, coll) => acc.concat(spaces[coll].features), [] as types.SpaceFeature[]);
        // tslint:disable-next-line: max-line-length
        const connectionFeatures = Object.keys(connections).reduce((acc, coll) => acc.concat(connections[coll].features), [] as types.ConnectionFeature[]);

        await commit('SET_BUILDING_FEATURES', building.features);
        await commit('SET_LEVEL_FEATURES', levelFeatures);
        await commit('SET_OBSTRUCTION_FEATURES', obstructionFeatures);
        await commit('SET_SPACE_FEATURES', spaceFeatures);
        await commit('SET_CONNECTION_FEATURES', connectionFeatures);

        // Save icon images locally
        const poiManualImages = manualImages.map((item) => {
          return item.imageURL;
        }).filter((link) => link.length > 0);

        const poiManualImagesMediaURLArray =
          await downloadManager.downloadFileArray(poiManualImages);

        const poiManualImagesWithMedia = manualImages.map((item) => {
          const downloadedItem = poiManualImagesMediaURLArray.find((media) => media.fileURL === item.imageURL);

          item.mediaDataURL = downloadedItem ? downloadedItem.fileSystemURL.toString() : null;

          return item;
        });

        pois.forEach((poi) => {
          const manualImageObject = poiManualImagesWithMedia.find((manualImage) => manualImage.id === poi.id);

          if (manualImageObject && manualImageObject.mediaDataURL) {
            poi.mediaDataURL = manualImageObject.mediaDataURL;
          }

          // tslint:disable-next-line: max-line-length
          const manualDescriptionObject = manualDescriptions.find((manualDescription) => manualDescription.id === poi.id);

          if (manualDescriptionObject && manualDescriptionObject.description) {
            poi.description = manualDescriptionObject.description;
          }

          const targetSpaces = poi.spaces;

          const navNodesArray: types.POINavNode[] = [];

          targetSpaces.forEach((targetSpace) => {
            const space = spaceFeatures.find((feature) => feature.properties.id === targetSpace) || null;

            // tslint:disable-next-line: max-line-length
            let targetNode = space ? navNodes.find((feature) => insideRobust(feature.geometry.coordinates, space.geometry.coordinates[0]) && feature.properties.level === space.properties.level) : null;

            if (targetNode === undefined) {
              targetNode = navNodes.find((feature) => feature.properties.id === `n_${targetSpace}`) || null;
            }

            if (targetNode) {
              navNodesArray.push({
                space: targetSpace,
                navNode: targetNode.properties.id,
              });
            }
          });

          poi.navNodes = navNodesArray;
        });

        await commit('SET_POIS', pois);
      } catch (e) {
        logError(e);

        return;
      }
    },
  },
};
