const VERSION = "manifest_version";

const cacheFiles = [
  "./index.html",
  "./manifest.json",
  "./icon_128.png",
  "./img/144x144.png",
  "./img/512x512.png",
];

self.addEventListener("install", e => {
  e.waitUntil(
    caches.open(VERSION).then((cache) => {
       cache.addAll(cacheFiles)
    })
  );
});

self.addEventListener('activate', e => {
  e.waitUntil(
      caches.keys().then(keys => {
          keys.forEach(element => {
              if (element !== VERSION) {
                  caches.delete(element);
              }
          });
      })
  );
});

self.addEventListener('fetch', e => {
  e.respondWith(
    caches.match(e.request).then((response) => {
      return response || fetch(e.request);
    })
  )
});