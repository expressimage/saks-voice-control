"""
Records a Bitbucket Pipelines build as a deploy in Rollbar.
"""

from __future__ import print_function
import os
import pipes
import subprocess
import sys


DEFAULT_ENDPOINT = 'https://api.rollbar.com/api/1/deploy/'
DEFAULT_MASTER_ENVIRONMENT = 'development'
LOCAL_USERNAME = 'Bitbucket Pipelines'


def endpoint():
    value = os.environ.get('ROLLBAR_ENDPOINT')

    if not value:
        return DEFAULT_ENDPOINT

    if value.endswith('deploy/'):
        # valid deploy endpoint - return it
        return value
    else:
        # not valid. Likely either has a path like /api/1/ or /api/1/item/
        if value.endswith('item/'):
            return value[:-len('item/')] + 'deploy/'
        elif value.endswith('/api/1/'):
            return value + 'deploy/'
        else:
            # use the specified endpoint without modification
            return value


def branch_variable_name(branch):
    return 'ROLLBAR_ENVIRONMENT_%s' % branch
    

def environment_for_branch(branch):
    value = os.environ.get(branch_variable_name(branch))
    
    # for the 'master' branch, default to 'production' if the variable is not set
    if branch == 'master' and value is None:
        value = DEFAULT_MASTER_ENVIRONMENT

    return value


def build_comment(branch, repo_slug, repo_owner):
    return 'Branch: %s\n\nRepo: %s/%s' % (
        branch, repo_slug, repo_owner)


def record_deploy(revision, environment, local_username, comment):
    access_token = os.environ.get('ROLLBAR_ACCESS_TOKEN')
    url = endpoint()
    
    # use curl so we avoid python dependencies
    args = ['curl', '-X', 'POST', url]
    params = {
        'access_token': access_token,
        'revision': revision,
        'environment': environment,
        'local_username': local_username,
        'comment': comment,
    }
    for name, value in params.items():
        args.append('-F')
        args.append('%s=%s' % (name, value))

    print("making curl call: %s" % args)
    subprocess.call(args)
    print("done")


def main():
    # built-in bitbucket variables
    revision = os.environ.get('BITBUCKET_COMMIT')
    branch = os.environ.get('BITBUCKET_BRANCH')
    repo_slug = os.environ.get('BITBUCKET_REPO_SLUG')
    repo_owner = os.environ.get('BITBUCKET_REPO_OWNER')

    access_token = os.environ.get('ROLLBAR_ACCESS_TOKEN')
    if not access_token:
        print("Error: Not reporting build to Rollbar because the ROLLBAR_ACCESS_TOKEN environment variable is not set.")
        sys.exit(1)

    environment = environment_for_branch(branch)
    if environment:
        comment = build_comment(branch, repo_slug, repo_owner)
        local_username = LOCAL_USERNAME

        record_deploy(revision, environment, local_username, comment)
    else:
        print("Skipping report to Rollbar because there is no environment configured for the current branch (%s). To report deploys in this environment, set the Bitbucket Pipelines environment variable name %s to the name of a Rollbar environment" % (branch, branch_variable_name(branch)))


if __name__ == '__main__':
    main()

