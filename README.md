# Saks Fifth Avenue Wayfinding Kiosk Application

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build-production
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## PWA Link
https://saks-voice-control.preview.expressimage.digital/<BRANCH_NAME>/index.html (For Default)

https://saks-voice-control.preview.expressimage.digital/<DEPLOY_ENV>/index.html