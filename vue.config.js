const RollbarSourceMapPlugin = require('rollbar-sourcemap-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const S3Plugin = require('webpack-s3-plugin');

const branchName = process.env.BITBUCKET_BRANCH;
const branchExtensionId = process.env[`${branchName}_PUBLISHING_EXTENSION_ID`];

const buildMode = process.env.BUILD_MODE;

module.exports = {
  publicPath:'./',
  pwa: {
    iconPaths: {
      favicon32: './icon_128.png',
      favicon16: './icon_128.png',
      appleTouchIcon: './icon_128.png',
    },
  },
  devServer: {
    https: true,
  },
  pages: {
    index: {
      // entry for the page
      entry: 'src/main.ts',
      // the source template
      template: 'public/index.html',
      // output as dist/index.html
      filename: 'index.html',
    },
    webviewParent: {
      // entry for the page
      entry: 'src/webviewParent/main.ts',
      // the source template
      template: 'public/main.html',
      // output as dist/index.html
      filename: 'main.html',
    },
    textToPhone: {
      // entry for the page
      entry: 'src/ttpmain.ts',
      // the source template
      template: 'public/ttpIndex.html',
      // output as dist/ttpIndex.html
      filename: 'ttpIndex.html',
    },
  },
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.vue', '.json'],
    },
    module: {
      noParse: /iconv-loader\.js/,
      rules: [
        {
          test: /\.js$/,
          loader: "imports-loader?define=>false",
        },
        {
          test: /\.(jpg|png|gif)$/,
          loader: 'image-webpack-loader',
          // Specify enforce: 'pre' to apply the loader
          // before url-loader/svg-url-loader
          // and not duplicate it in rules with them
          enforce: 'pre'
        },
        {
          test: /\.css$/,
          use: [ "style-loader", "css-loader", "less-loader" ]
        }
      ],
    },
    plugins: [],
  },
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule
    .use('vue-svg-loader')
    .loader('vue-svg-loader');
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/styles.module.scss";`,
      },
    },
  },
};

if (branchName && branchExtensionId) {
  if (process.env.VUE_APP_VERSION !== '<APP_VERSION>') {
    module.exports.configureWebpack.plugins.push(new RollbarSourceMapPlugin({
      accessToken: process.env.ROLLBAR_SOURCEMAP_ACCESS_TOKEN,
      version: process.env.VUE_APP_VERSION,
      publicPath: process.env.PUBLIC_PATH,
    }));
  }

  if (buildMode && buildMode === 'chrome') {
    module.exports.configureWebpack.plugins.push(
      new ZipPlugin({
        filename: 'nextPatch.zip',
      }),
      new S3Plugin({
        // Only upload zip file
        include: /.*\.zip/,
        // s3Options are required
        s3Options: {
          accessKeyId: process.env.PUBLISHING_AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.PUBLISHING_AWS_SECRET_ACCESS_KEY,
          region: process.env.PUBLISHING_AWS_REGION,
        },
        s3UploadOptions: {
          Bucket: process.env.PUBLISHING_S3_BUCKET,
          ACL: 'private',
          Key: `ingest/${branchExtensionId}/nextPatch.zip`,
        },
      }),
      new S3Plugin({
        // Only upload zip file
        exclude: /.*\.zip/,
        // s3Options are required
        s3Options: {
          accessKeyId: process.env.PUBLISHING_AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.PUBLISHING_AWS_SECRET_ACCESS_KEY,
          region: process.env.PUBLISHING_AWS_REGION,
        },
        s3UploadOptions: {
          Bucket: process.env.PUBLISHING_S3_BUCKET,
          ACL: 'private',
        },
        basePath: `web/${branchExtensionId}`,
      }),
    );
  }
}
